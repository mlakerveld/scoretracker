# README #

## How to install ##

### Database ###
1. Create a database for ScoreTracker to use
2. Run the queries in the file **database-create.sql**
3. Insert players into the player table

### Config file ###

1. Rename **config.example.php** to **config.php**
2. Set your Database credentials in the config file
3. Add users and passwords for people to insert scores with to the **$allowedUsers** array in the format `'username' => 'sha512hashofpassword'`

## How to use ##
You can add scores at **insert.php** using the users as defined above. The rest should be self-explanatory.
