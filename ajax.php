<?php
header('Content-Type: application/json');

//Init
require_once('config.php');

//Get POST vars, or use defaults instead
$type   = isset($_POST['type'])     ? $_POST['type']    : 'month';
$month  = isset($_POST['month'])    ? $_POST['month']   : date('n');
$year   = isset($_POST['year'])     ? $_POST['year']    : date('Y');

//Determine start and end date based on filters
$startdate = null;
$enddate = null;
if($type == 'month') {
    $date       = $year.'-'.$month.'-01';
    $startdate  = date('Y-m-01', strtotime($date));
    $enddate    = date('Y-m-t', strtotime($date));
} elseif($type == 'year') {
    $startdate  = $year.'-01-01';
    $enddate    = $year.'-12-31';
}

//Retrieve all the needed data from the database
$database = new Database();
$players            = $database->getPlayers();
$playdates          = $database->getPlayDates($startdate, $enddate);

//If there weren't any games stop the script
if(!sizeof($playdates)) {
    echo(json_encode(array(
        'played' => 0
    )));
    die();
}

$results            = array();
$cumulativeResults  = array();
foreach($playdates as $playdate) {
    $cumulativeResults[$playdate]   = $database->getCumulativeResults($startdate, $playdate);
    $results[$playdate]             = $database->getResults($playdate);
}

//Retrieve final item (which contains the percentages for the bar graph)
$finalResult = end($cumulativeResults);
reset($cumulativeResults);

//Parse the data for the bar graph
$barCategories = array();
$barData = array();
foreach($finalResult as $playerName => $result) {
    $barCategories[] = $playerName;
    $barData[] = floatval($result['percentage']);
}

//Parse the data for the line graph
$lineSeries = array();
foreach($players as $playerName) {
    $playerLineSeries = array(
        'name' => $playerName,
        'data' => array()
    );
    foreach($cumulativeResults as $result) {
        if(isset($result[$playerName])) {
            $playerLineSeries['data'][] = floatval($result[$playerName]['percentage']);
        } else {
            $playerLineSeries['data'][] = null;
        }
    }
    $lineSeries[] = $playerLineSeries;
}

//Parse the data for the table
$defaultTableData =
$tableData = array();

//Parse the total row
$tableRow = array();
$tableRow['name'] = 'Totaal';
foreach($players as $playerName) {
    if (isset($finalResult[$playerName])) {
        $tableRow['data'][] = $finalResult[$playerName];
    } else {
        $tableRow['data'][] = array(
            'played' => 0,
            'won' => 0,
            'percentage' => '-'
        );
    }
}
$tableData[] = $tableRow;

//Parse the date rows
foreach(array_reverse($playdates) as $playdate) {
    $tableRow = array();
    $tableRow['name'] = $playdate;
    foreach($players as $playerName) {
        $tableRow['data'][] = array(
            'played' => isset($results[$playdate][$playerName]) ? $results[$playdate][$playerName]['played'] : 0,
            'won' => isset($results[$playdate][$playerName]) ? $results[$playdate][$playerName]['won'] : 0,
            'percentage' => isset($cumulativeResults[$playdate][$playerName]) ? $cumulativeResults[$playdate][$playerName]['percentage'] : '-'
        );
    }
    $tableData[] = $tableRow;
}

//Return the results
echo(json_encode(array(
    'played' => 1,
    'bar_categories' => $barCategories,
    'bar_data' => $barData,
    'line_categories' => $playdates,
    'line_series' => $lineSeries,
    'table_data' => $tableData
)));
?>