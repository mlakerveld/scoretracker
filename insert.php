<?php
//Init
require_once('config.php');
$database = new Database();

//Retrieve players
$players = $database->getPlayers();

//Handle POST if the form was submitted
if(isset($_POST['add'])) {
    //Check if user is valid
    $validUser = false;
    if(isset($allowedUsers[$_POST['name']])) {
        if(hash('sha512', $_POST['pass']) == $allowedUsers[$_POST['name']]) {
            $validUser = true;
        }
    }

    //If user is invalid stop the script
    if(!$validUser) {
        die('nee');
    }

    //Insert the result into database
    $playedIds = array();
    foreach($players as $playerId => $playerName) {
        if($_POST['played'][$playerId] != '' && intval($_POST['played'][$playerId]) > 0) {
            $playedIds[] = $playerId;
            $database->insertResult($playerId, $_POST['date'], intval($_POST['played'][$playerId]), intval($_POST['won'][$playerId]));
        }
    }

    //Update player order
    $order = json_decode(file_get_contents('order.json'));
    $orderMoveToEnd = array();
    foreach($order as $index => $playerId) {
        if(in_array($playerId, $playedIds)) {
            unset($order[$index]);
            $orderMoveToEnd[] = $playerId;
        }
    }
    //Move played to end
    foreach($orderMoveToEnd as $playerId) {
        $order[] = $playerId;
    }
    //Add new players
    foreach($playedIds as $playerId) {
        if(!in_array($playerId, $orderMoveToEnd)) {
            $order[] = $playerId;
        }
    }
    //Write new order
    file_put_contents('order.json', json_encode(array_values($order)));
}

?>
<html>
    <head>
        <title>Tafelvoetbal Scores</title>
    </head>
    <body>
        <form method="post" action="">
            <?=(isset($_POST['add']) ? 'DONE' : '')?>
            <table>
                <tr>
                    <td>User</td>
                    <td><input type="text" name="name" value=""></td>
                    <td><input type="password" name="pass" value=""></td>
                </tr>
                <tr>
                    <td>Datum</td>
                    <td><input type="text" name="date" value="<?=date('Y-m-d')?>"></td>
                </tr>
                <? foreach($players as $playerId => $playerName) { ?>
                    <tr>
                        <td><?=$playerName?></td>
                        <td><input type="text" name="played[<?=$playerId?>]" value="" placeholder="Gespeeld"></td>
                        <td><input type="text" name="won[<?=$playerId?>]" value="" placeholder="Gewonnen"></td>
                    </tr>
                <? } ?>
            </table>
            <input type="submit" value="Toevoegen" name="add">
        </form>
    </body>
</html>