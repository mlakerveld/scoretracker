$(function() {
    //Show the possible filters for the selected type of graphs
    $('input[name="type"]').change(function() {
        if($(this).is(':checked')) {
            var monthField = $('select[name="month"]');
            var yearField = $('select[name="year"]');
            switch($(this).val()) {
                case "month":
                    monthField.show();
                    yearField.show();
                    break;
                case "year":
                    monthField.hide();
                    yearField.show();
                    break;
                case "total":
                    monthField.hide();
                    yearField.hide();
                    break;
            }
        }
    });

    //Handle the button click
    $('#refresh-graphs').click(function() {
        loadData();
    });

    //Initial load graphs when the page is done loading
    loadData();
});

//Function to show data for the selected period
function loadData() {
    $.ajax({
        url: 'ajax.php',
        type: 'POST',
        data: {
            type: $('input[name="type"]:checked').val(),
            month: $('select[name="month"]').val(),
            year: $('select[name="year"]').val()
        },
        success: function(data) {
            $('.graph').toggle(data.played == 1);
            $('#result-table').toggle(data.played == 1);
            $('#nogames').toggle(data.played == 0);
            if(data.played == 1) {
                //Show the data in the GUI
                showBarGraph(data.bar_categories, data.bar_data);
                showLineGraph(data.line_categories, data.line_series);
                showTable(data.table_data);
            }
        }
    });
}

//Function to show the bar graph
function showBarGraph(categories, data) {
    $("#bar-graph").highcharts({
        chart: { type: 'bar' },
        title: { text: 'Percentage' },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: { enabled: false },
            max: 100,
            labels: { formatter: function() { return this.value+'%'; } }
        },
        series: [{
            name: 'Percentage',
            data: data
        }],
        legend: { enabled: false },
        credits: { enabled: false }
    });
}

//Function to show the line graph
function showLineGraph(categories, series) {
    $("#line-graph").highcharts({
        title: { text: 'Percentageverloop' },
        xAxis: {
            categories: categories,
            labels: { enabled: false },
            showEmpty: false
        },
        yAxis: {
            title: { enabled: false },
            min: 0,
            max: 100,
            labels: { formatter: function() { return this.value+'%'; } }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: { line: { marker: { enabled: categories.length <= 31 } } },
        series: series,
        credits: { enabled: false }
    });
}

//Function to show the table
function showTable(data) {
    var tableElement = $('#result-table');

    //Remove old rows
    $('.result-row').remove();

    //Add new rows
    for(var i = 0; i < data.length; i++) {
        var row = data[i];
        var rowHTML = '<tr class="result-row">';
        rowHTML += '<th>'+row.name+'</th>';

        for(var j = 0; j < row.data.length; j++) {
            var cell = row.data[j];
            rowHTML += '<td>'+cell.played+'</td>';
            rowHTML += '<td>'+cell.won+'</td>';
            rowHTML += '<td>'+cell.percentage+(cell.percentage == '-' ? '' : '%')+'</td>';
        }

        rowHTML += '</tr>';
        tableElement.append(rowHTML);
    }
}

//Set the highcharts theme
Highcharts.theme = {
    colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
    chart: { backgroundColor: '#373838' },
    title: { style: { color: '#fff' } },
    subtitle: { style: { color: '#fff' } },
    legend: {
        itemStyle: {  color: '#fff' },
        itemHoverStyle:{ color: '#fff' }
    },
    xAxis: {
        title: { style: { color: '#fff' } },
        labels: { style: { color: '#fff' } },
        lineColor: '#fff'
    },
    yAxis: {
        title: { style: { color: '#fff' } },
        labels: { style: { color: '#fff' } },
        lineColor: '#fff'
    }
};
Highcharts.setOptions(Highcharts.theme);