<?php
class Database {
    private $pdo;

    /**
     * Initiate the database connection
     */
    public function __construct() {
        $dsn = 'mysql:dbname='.DB_NAME.';host='.DB_HOST;
        $this->pdo = new PDO($dsn, DB_USER, DB_PASS);
    }

    /**
     * Wrapper function to prepare and execute a query
     * @param string $sql
     * @param array $values
     * @return PDOStatement
     */
    private function query($sql, $values = array()) {
        $pdoResult = $this->pdo->prepare($sql);
        $pdoResult->execute($values);
        return $pdoResult;
    }

    /**
     * Retrieve all the active players
     * @return array
     */
    public function getPlayers() {
        $sql = "
              SELECT id, name
              FROM player
              WHERE inactive = 0
              ORDER BY name ASC";
        $pdoResult = $this->query($sql);
        return $pdoResult->fetchAll(PDO::FETCH_KEY_PAIR);
    }

    /**
     * Get all the dates on which was played in the given period
     * @param string $startdate
     * @param string $enddate
     * @return string[]
     */
    public function getPlayDates($startdate, $enddate) {
        $values = array();
        $sql = "
              SELECT result.date
              FROM result
              JOIN player ON result.player_id = player.id AND player.inactive = 0
              WHERE 1=1
        ";

        if($startdate != null) {
            $values['startdate'] = $startdate;
            $sql .= " AND result.date >= :startdate";
        }
        if($enddate != null) {
            $values['enddate'] = $enddate;
            $sql .= " AND result.date <= :enddate";
        }

        $sql .= "
            GROUP BY result.date
            ORDER BY result.date ASC";

        $pdoResult = $this->query($sql, $values);
        return $pdoResult->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * Get the cumulative results from each player in the given period
     * @param string $startdate
     * @param string $enddate
     * @return array
     */
    public function getCumulativeResults($startdate, $enddate) {
        $sql = "
            SELECT
              player.name,
              SUM(result.played) AS played,
              SUM(result.won) AS won,
              ROUND(SUM(result.won)/SUM(result.played)*100 , 2) AS percentage
            FROM result
            JOIN player ON result.player_id = player.id AND player.inactive = 0
            WHERE 1=1";

        if($startdate != null) {
            $values['startdate'] = $startdate;
            $sql .= " AND result.date >= :startdate";
        }
        if($enddate != null) {
            $values['enddate'] = $enddate;
            $sql .= " AND result.date <= :enddate";
        }

        $sql .= "
            GROUP BY result.player_id
            ORDER BY percentage DESC";
        $oRes = $this->query($sql, $values);
        return $oRes->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);
    }

    /**
     * Get the results from each player on the given date
     * @param string $date
     * @return array
     */
    public function getResults($date) {
        $values['date'] = $date;
        $sql = "
            SELECT
              player.name,
              SUM(result.played) AS played,
              SUM(result.won) AS won
            FROM result
            JOIN player ON result.player_id = player.id AND player.inactive = 0
            WHERE date = :date
            GROUP BY result.player_id";
        $oRes = $this->query($sql, $values);
        return $oRes->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);
    }

    /**
     * Insert a result
     * @param int $iPlayerId
     * @param string $sDate
     * @param int $iPlayed
     * @param int $iWon
     */
    public function insertResult($iPlayerId, $sDate, $iPlayed, $iWon) {
        $values = array(
            ':date'         => $sDate,
            ':player_id'    => $iPlayerId,
            ':played'       => $iPlayed,
            ':won'          => $iWon
        );
        $sql = "
            INSERT INTO result
            VALUES (:date, :player_id, :played, :won)";
        $this->query($sql, $values);
    }
}